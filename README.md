# Api-Doc-Builder

##### 本專案使用 node 環境：

```text
node^20.9.0
```

##### 本專案使用技術組合：

```text
Angular^17.1.0
ngx-print^1.5.1
```

##### 其他说明：

- 使用 GitLab CI 持续整合 (GitLab CI Runner in Docker)
- 使用 ESLint 程式碼規範與風格檢查

## 專案目的[^1]

[^1]: 專案尚未完成

該專案工具允許使用者，將自訂義 JSON 規格轉換為 API 規格文件，簡化 API 規格文件的創建過程，並確保規格文件的一制性和準確性

## 指令

```bash
# 安裝依賴
npm i

# 開發模式(本地)
npm run start

# 開發模式(docker)
npm run start_docker

# 測試
npm run test

# 檢查原始碼格式
npm run lint

# 檢查排版
npm run format

# 自動排版
npm run format-write

# 打包
npm run build
```

## 工具使用步驟

1. **點按『 選擇檔案 』，選擇json檔案（多選）**
   ![structure](./img/001.png)

   #

2. **輸入功能名稱後，點按『 render 』**
   ![structure](./img/002.png)

   #

3. **出現預覽畫面**
   ![structure](./img/003.png)

   #

4. **點按『 print 』，可列印文件**
   ![structure](./img/004.png)

## json 格式定義規範

請參照以下格式設計 json 檔案，一支 API 為一個檔案

```text
{
  "infoConfig": {
    "taskId": string,          // API 代號
    "name": string,            // 功能名稱
    "serviceType": string      // 服務分類
  },

  "reqBodyConfig": {
    // 資料型態：String
    [name]: {                  // 欄位名稱
      "description": string,   // 欄位說明
      "memo": string,          // 備註
      "value": string          // api sample value
    },

    // 資料型態：Object
    [name]: {                  // 欄位名稱
      "description": string,   // 欄位說明
      "memo": string,          // 備註
      "data": object           // 物件內容（可繼續擴展子屬性，增加層級）
    },

    // 資料型態：Object[Array]
    [name]: {                  // 欄位名稱
      "description": string,   // 欄位說明
      "memo": string,          // 備註
      "list": object           // 陣列物件內容（可繼續擴展子屬性，增加層級）
    }
  },

  "resBodyConfig": {...}       // 同 reqBodyConfig 結構
}
```

## json 格式範例

詳請參考檔案

- [test01](./test01.json)
- [test02](./test02.json)

```json
{
  "infoConfig": {
    "taskId": "XXXXX001_02",
    "name": "查詢",
    "serviceType": "Fcy"
  },

  "reqBodyConfig": {
    "payerName": {
      "description": "付款人名稱",
      "memo": "",
      "value": "噴火龍"
    }
  },

  "resBodyConfig": {
    "caseId": {
      "description": "案件編號",
      "memo": "",
      "value": "00000001"
    },
    "payerInfo": {
      "description": "付款人資訊",
      "memo": "若無為空物件",
      "data": {
        "payerName": {
          "description": "付款人名稱",
          "memo": "",
          "value": "皮卡丘"
        }
      }
    },
    "totalAmount": {
      "description": "購買金額",
      "memo": "",
      "list": {
        "amt": {
          "description": "幣別",
          "memo": "",
          "value": "USD"
        },
        "ccy": {
          "description": "金額",
          "memo": "未格式化",
          "value": "100000"
        }
      }
    }
  }
}
```
